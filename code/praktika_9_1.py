def mano_funkcija_9_1(mano_sarasas):
    """
    Funkcija, kuri patikrina sąrašą
    Versija 0.1 2021-11-08T1652 AU
    :param:  None
    :return: None
    """

    ## pradiniai duomenys
    # mano_sarasas = [1, 2, 3, 4, 5] # test case 1
    # mano_sarasas = []              # test case 2


    ## duomenų sąrašo tikrinimas
    print(len(mano_sarasas))


    ## pačių duomenų tikrinimas
    if(len(mano_sarasas) > 0 and mano_sarasas is not None):
        print(mano_sarasas[0])
        print(mano_sarasas[len(mano_sarasas)-1])

        ## duomenų peržiūra
        for mano_elementas in mano_sarasas:
            print(mano_elementas)
        
        ## v2: list comprehension
        mano_sarasas_2 = [mano_elementas  for x in mano_sarasas]
        print(mano_sarasas_2)
